libclass-load-xs-perl (0.10-2+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 06 Apr 2023 11:33:59 +0000

libclass-load-xs-perl (0.10-2) unstable; urgency=medium

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * debian/copyright: replace tabs with spaces / remove trailing
    whitespace.
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on libclass-load-perl.
    + libclass-load-xs-perl: Drop versioned constraint on libclass-load-perl in
      Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 11 Jun 2022 22:39:09 +0100

libclass-load-xs-perl (0.10-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sat, 20 Feb 2021 00:43:33 +0000

libclass-load-xs-perl (0.10-1) unstable; urgency=medium

  * Import upstream version 0.10
    Fixes "FTBFS: t/012-without-implementation.t failure"
    (Closes: #867984)
  * Update years of packaging and third-party copyright.
  * Update build dependencies.
  * Declare compliance with Debian Policy 4.0.0.

 -- gregor herrmann <gregoa@debian.org>  Mon, 10 Jul 2017 23:07:30 +0200

libclass-load-xs-perl (0.09-2) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ intrigeri ]
  * Enable all hardening build flags.
  * Declare compliance with Standards-Version 3.9.8.

 -- intrigeri <intrigeri@debian.org>  Tue, 05 Jul 2016 19:51:54 +0000

libclass-load-xs-perl (0.09-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Import upstream version 0.09
  * Update years of packaging copyright.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Sun, 28 Jun 2015 14:30:19 +0200

libclass-load-xs-perl (0.08-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
  * Update years of copyright, and file locations in debian/copyright.
  * Install new CONTRIBUTING file.
  * Use debhelper 9.20120312 to get all hardening flags.
  * Drop build dependency on Module::Build.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sat, 15 Feb 2014 15:41:56 +0100

libclass-load-xs-perl (0.06-1) unstable; urgency=low

  * New upstream release

 -- Alessandro Ghedini <ghedo@debian.org>  Tue, 16 Oct 2012 13:15:08 +0200

libclass-load-xs-perl (0.05-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Alessandro Ghedini ]
  * New upstream release
  * Email change: Alessandro Ghedini -> ghedo@debian.org
  * Bump Standards-Version to 3.9.4 (no changes needed)
  * Versioned (build) depends on libclass-load-perl
  * Update copyright to Copyright-Format 1.0

 -- Alessandro Ghedini <ghedo@debian.org>  Tue, 09 Oct 2012 11:22:00 +0200

libclass-load-xs-perl (0.04-1) unstable; urgency=low

  * New upstream release.
  * Update year of upstream copyright.
  * Switch to debhelper compatibility level 9 to pass CFLAGS to
    Makefile.PL/Build.PL.
  * Add /me to Uploaders.
  * Make (build) dependency on libclass-load-perl versioned. Add build
    dependencies on libmodule-implementation-perl, libtest-requires-perl,
    libtest-without-module-perl.

 -- gregor herrmann <gregoa@debian.org>  Sat, 11 Feb 2012 20:12:31 +0100

libclass-load-xs-perl (0.03-1) unstable; urgency=low

  * Initial Release (Closes: #648978)

 -- Alessandro Ghedini <al3xbio@gmail.com>  Tue, 22 Nov 2011 14:54:30 +0100
